<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
	return View::make('hello');
});

Route::get("api/event", [
    "as"   => "contact/index",
    "uses" => "ApiContactController@index"
]);
Route::post("api/contact", [
    "as"   => "contact/store",
    "uses" => "ApiContactController@store"
]);
Route::get("api/contact/{contact}", [
    "as"   => "contact/show",
    "uses" => "ApiContactController@show"
]);
Route::put("api/contact/{contact}", [
    "as"   => "contact/update",
    "uses" => "ApiContactController@update"
]);
Route::delete("api/contact/{contact}", [
    "as"   => "contact/destroy",
    "uses" => "ApiContactController@destroy"
]);