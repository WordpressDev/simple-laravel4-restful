<?php

class Contact extends Eloquent {

    protected $table = "contact";
    protected $guarded = ["id"];
    protected $softDelete = true;

    public function user()
    {
        return $this->belongsTo("User");
    }
}